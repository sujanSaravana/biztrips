# Test Concept

## Objective
The objective of the test concept is to ensure the quality and reliability of the "biztrips" application through comprehensive testing. This includes validating the functionality of both the frontend (React) and the backend (Json Server Port 3001) components.

## Test Strategy

### Functional Areas
- CRUD Operations for trips (GET, POST, DELETE, PUT)
- Automated Project Testing via GitLab CI/CD

## Roles and Responsibilities

### Test Team
- Sujan Saravana
- Tarek Schütz

### Task Allocation
- Sujan Saravana: Backend Tests, GitLab CI/CD Pipeline Configuration
- Tarek Schütz: Frontend Tests, GitLab CI/CD Pipeline Configuration

## Infrastructure

### Test Environment
- GitLab CI/CD Pipeline
- Netlify
- Local Development Environment

## Test Cases

### Frontend Tests

#### Add Trips

**Objective:** Verify if trips can be successfully added to the Wishlist.

**Test Steps:**
1. Add a trip to the Wishlist.
2. Verify that the trip is correctly added to the Wishlist.
3. Confirm that the Wishlist reflects the added trip.

#### Render Wishlist Items

**Objective:** Ensure that Wishlist items are properly rendered on the frontend.

**Test Steps:**
1. Add multiple trips to the Wishlist.
2. Check if all added trips are visible and rendered on the Wishlist component.

#### Render Empty Wishlist Message

**Objective:** Confirm that the "Wishlist is empty" message is displayed when the Wishlist has no items.

**Test Steps:**
1. Remove all items from the Wishlist.
2. Check if the message "Wishlist is empty" is visible on the frontend.

#### Remove From Wishlist

**Objective:** Validate if the "delete" function is triggered when the remove button is clicked.

**Test Steps:**
1. Add a trip to the Wishlist.
2. Click on the remove button for the added trip.
3. Confirm that the "delete" function is called.
4. Confirm that the removed trip is no longer displayed in the Wishlist.

#### Empty Wishlist

**Objective:** Confirm that the "empty wishlist" function is invoked when the button is clicked.

**Test Steps:**
1. Add multiple trips to the Wishlist.
2. Click on the "Empty Wishlist" button.
3. Verify that the "empty wishlist" function is triggered.
4. Confirm that the Wishlist is now empty, and no items are displayed.

### Backend Tests

1. **Get Trips**
   - Tests the GET request to the /trips interface to ensure the application can successfully retrieve all trips.
     1. Send a `GET` request to the `/trips` interface.
     2. Verify the response status code (should be 200 OK).
     3. Check the response data format.

2. **Get Trip by ID**
   - Tests the GET request to the /trips/{id} interface to ensure the application can successfully retrieve a single trip.
     1. Choose a valid Trip ID.
     2. Send a `GET` request to the `/trips/{id}` interface.
     3. Verify the response status code (should be 200 OK).
     4. Check the response data format.

3. **Post Trips**
   - Tests the POST request to the /trips interface to ensure the application can successfully create a new trip.
     1. Create a trip with valid data.
     2. Send a `POST` request to the `/trips` interface with the created data.
     3. Verify the response status code (should be 201 Created).
     4. Check if the created data is present in the database.

4. **Delete Trip**
   - Tests the DELETE request to the /trips/{id} interface to ensure the application can successfully delete a trip.
     1. Choose a valid Trip ID.
     2. Send a `DELETE` request to the `/trips/{id}` interface.
     3. Verify the response status code (should be 200 OK).
     4. Retrieve the same trip again to ensure it is no longer present.

5. **Put Trip**
   - Tests the PUT request to the /trips/{id} interface to ensure the application can successfully update a trip.
     1. Choose a valid Trip ID.
     2. Update the data of the selected trip.
     3. Send a `PUT` request to the `/trips/{id}` interface with the updated data.
     4. Verify the response status code (should be 200 OK).
     5. Retrieve the updated trip again and check if the updates were applied.

## Defect Handling

### Defect Identification
1. Failed Tests
2. Website Navigation

### Defect Resolution
1. Analyze the failed test case
2. Identify the relevant code
3. Correct the code
4. Corrected code will undergo retesting
