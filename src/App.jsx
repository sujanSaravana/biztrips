import React, { useState } from "react";
import "./App.css";

import Footer from "./Footer";
import Header from "./Header";
import TripList from "./components/TripList";
import Wishlist from "./components/Wishlist";

export default function App() {
  const [wishlist, setWishlist] = useState([]);

  function addToWishlist(trip) {
    const { id, title, description, startTrip, endTrip } = trip;
    setWishlist((prevWishlist) => {
      const tripInWishlist = prevWishlist.find((t) => t.id === id);
      if (tripInWishlist) {
        return prevWishlist;
      } else {
        return [...prevWishlist, { id, title, description, startTrip, endTrip }];
      }
    });
  }

  function removeFromWishlist(item) {
    setWishlist((prevWishlist) => prevWishlist.filter((t) => t.id !== item.id));
  }

  function clearWishlist() {
    setWishlist([]);
  }

  return (
    <>
      <div>
        <Header />
        <main>
          <h1>Welcome to biztrips Happy new Year-react - 2024</h1>
          <Wishlist
            wishlist={wishlist}
            removeFromWishlist={removeFromWishlist}
            clearWishlist={clearWishlist}
          />
          <TripList addToWishlist={addToWishlist} />
        </main>
      </div>
      <Footer />
    </>
  );
}
