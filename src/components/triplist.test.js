import React from 'react';
import userEvent from '@testing-library/user-event';
import { render, fireEvent, screen, waitFor } from '@testing-library/react';
import TripList from './TripList'; // replace with your actual component file path
import { testTrips } from './api';

describe('TripList', () => {
  const addToWishlistMock = jest.fn();

  beforeEach(() => {
    render(<TripList addToWishlist={addToWishlistMock} />);
  });

  test('renders trip list', () => {
    const tripElement = screen.getByText(/Triplist-Catalog/i);
    expect(tripElement).toBeInTheDocument();
  });

  test('renders trip items', () => {
    const firstTrip = testTrips[0];
    const tripElement = screen.getByText(new RegExp(firstTrip.title, 'i'));
    expect(tripElement).toBeInTheDocument();
  });

  test('calls addToWishlist when "Add to Wishlist" is clicked', () => {
    const firstTrip = testTrips[0];
    const addButtons = screen.queryAllByText(/Add to Wishlist/i);
    
    expect(addButtons.length).toBeGreaterThan(0);

    fireEvent.click(addButtons[0]);
    expect(addToWishlistMock).toHaveBeenCalledWith(firstTrip);
  });

  test('filters trips by month', async () => {
    const selectElement = screen.getByLabelText(/Filter by Month:/i);
    userEvent.selectOptions(selectElement, ['1']); // Selects the option with value '1'

    const filteredTrips = testTrips.filter(t => new Date(t.startTrip).getMonth() === 0); // Months are 0-indexed in JavaScript Date

    await waitFor(() => {
      expect(screen.getByText(`Found ${filteredTrips.length} trips for the month of Jan`)).toBeInTheDocument();
    });
});
});