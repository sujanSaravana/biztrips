import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { testTrips } from "./api";

function TripList({ addToWishlist }) {
  const [month, setMonth] = useState("");
  const [trips] = useState(testTrips);
  const months = ["Jan", "Feb", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  const empty = (
    <section>
      <p className="alert alert-info">Triplist is empty</p>
    </section>
  );

  const filteredTrips = month
    ? trips.filter((trip) => {
        const startMonth = new Date(trip.startTrip).getMonth() + 1;
        return startMonth === parseInt(month);
      })
    : trips;

  return (
    <div className="container">
      <section>
        <h2 className="h4">Triplist-Catalog</h2>
        <section id="filters">
          <label htmlFor="month">Filter by Month:</label>
          <select
            id="month"
            value={month}
            onChange={(e) => setMonth(e.target.value)}
          >
            <option value="">All Months</option>
            {months.map((month, index) => (
              <option key={index} value={index + 1}>
                {month}
              </option>
            ))}
          </select>
          {month && (
            <h2>
              Found {filteredTrips.length}{" "}
              {filteredTrips.length === 1 ? "trip" : "trips"} for the month of{" "}
              {months[month - 1]} {/* Subtract 1 from month when using it as an index */}
            </h2>
          )}
        </section>
        <div className="row">
          {filteredTrips.length > 0 ? (
            filteredTrips.map((trip) => (
              <TripCard addToWishlist={addToWishlist} trip={trip} key={trip.id} />
            ))
          ) : (
            empty
          )}
        </div>
      </section>
    </div>
  );
}

function TripCard({ addToWishlist, trip }) {
  const { title, description } = trip;

  return (
    <div className="col-sm-6 col-md-4 col-lg-3">
      <figure className="card card-product">
        <div className="img-wrap">
          <img
            src={require(`../../public/images/items/${trip.id}.jpg`)}
            alt={title}
          />
        </div>
        <figcaption className="info-wrap">
          <h5 className="title">{title}</h5>
          <h6>Starts: {new Date(trip.startTrip).toLocaleString()}</h6>
          <h6>Ends: {new Date(trip.endTrip).toLocaleString()}</h6>
          <p className="card-text">{description}</p>
          <div className="info-wrap row">
            <button
              type="button"
              className="btn btn-link btn-outline"
              onClick={() => addToWishlist(trip)}
            >
              <i className="fa fa-shopping-cart" /> Add to Wishlist
            </button>
          </div>
        </figcaption>
      </figure>
    </div>
  );
}

export default TripList;