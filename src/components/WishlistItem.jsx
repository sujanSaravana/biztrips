import React from "react";

export default function WishlistItem({ removeFromWishlist, item }) {
  const { id, title, description, startTrip, endTrip } = item;

  return (
    <tr key={id}>
      <td>
        <figure className="media">
          <div className="img-wrap">
            <img
              className="img-thumbnail img-xs"
              src={`/images/items/${id}.jpg`}
              alt="img"
            />
          </div>
          <figcaption className="media-body">
            <h6 className="h6">{title}</h6>
            <dl className="dlist-inline small">
              <dt>{title}</dt>
              <dd>{description}</dd>
            </dl>
            <dl className="dlist-inline small">
              <dt>{startTrip.toLocaleString()}</dt>
              <dd>{endTrip.toLocaleString()}</dd>
            </dl>
          </figcaption>
        </figure>
      </td>
      <td className="price-wrap price"></td>
      <td className="text-right">
        <button className="btn btn-outline-success fa fa-heart fa-xs" />
        <i className="fa-regular fa-heart"></i>
        <button
          className="btn btn-outline-danger"
          onClick={() => removeFromWishlist(item)}
        >
          Delete Item
        </button>
      </td>
    </tr>
  );
}