import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Wishlist from "./Wishlist";

describe("Wishlist component", () => {
    const wishlist = [
        {
            id: 1,
            title: "Trip 1",
            description: "Description 1",
            startTrip: new Date("2022-01-01"),
            endTrip: new Date("2022-01-05"),
        },
        {
            id: 2,
            title: "Trip 2",
            description: "Description 2",
            startTrip: new Date("2022-02-01"),
            endTrip: new Date("2022-02-05"),
        },
    ];

    const removeFromWishlist = jest.fn();
    const clearWishlist = jest.fn();

    beforeEach(() => {
        render(
            <Wishlist
                wishlist={wishlist}
                removeFromWishlist={removeFromWishlist}
                clearWishlist={clearWishlist}
            />
        );
    });

    test("renders wishlist items", () => {
        const wishlistItems = screen.getAllByRole("row"); // Assuming each item is a row
        expect(wishlistItems.length).toBe(wishlist.length + 2); // +2 for the header and the total row
      });

    test("calls removeFromWishlist when delete button is clicked", () => {
        const deleteButtons = screen.getAllByText(/Delete/i); // Assuming the button text is "Delete"
        fireEvent.click(deleteButtons[0]);
        expect(removeFromWishlist).toHaveBeenCalledWith(wishlist[0]);
    });

    test("calls clearWishlist when empty wishlist button is clicked", () => {
        const emptyWishlistButton = screen.getByText(/Empty Wishlist/i); // Assuming the button text is "Empty Wishlist"
        fireEvent.click(emptyWishlistButton);
        expect(clearWishlist).toHaveBeenCalled();
    });

    test("renders empty wishlist message when wishlist is empty", () => {
        render(
            <Wishlist
                wishlist={[]}
                removeFromWishlist={removeFromWishlist}
                clearWishlist={clearWishlist}
            />
        );
        const emptyWishlist = screen.getByText("Wishlist is empty");
        expect(emptyWishlist).toBeInTheDocument();
    });
});