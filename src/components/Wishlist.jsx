import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

function WishlistItem({ removeFromWishlist, item }) {
  const { id, title, description, startTrip, endTrip, price } = item; // Assuming price is a property of item

  return (
    <tr key={id}>
      <td>
        <figure className="media">
          <div className="img-wrap">
            <img
              className="img-thumbnail img-xs"
              src={require(`../../public/images/items/${item.id}.jpg`)}
              alt="img"
            />
          </div>
          <figcaption className="media-body">
            <dl className="dlist-inline small">
              <dt>{title}</dt>
              <dd>{description}</dd>
            </dl>
            <dl className="dlist-inline small">
              <dt>{new Date(startTrip).toLocaleString()}</dt>
              <dd>{new Date(endTrip).toLocaleString()}</dd>
            </dl>
          </figcaption>
        </figure>
      </td>
      <td className="price-wrap price">{price}</td> {/* Display the actual price here */}
      <td className="text-right">
        <button
          className="btn btn-outline-danger"
          onClick={() => removeFromWishlist(item)}
        >
          delete Item
        </button>
      </td>
    </tr>
  );
}

export default function Wishlist({ wishlist, removeFromWishlist, clearWishlist }) {
  const itemsMapped = wishlist.map((item) => (
    <WishlistItem
      removeFromWishlist={removeFromWishlist}
      item={item}
      key={item.id}
    />
  ));

  const emptyMessage = (
    <p className="alert alert-info">Wishlist is empty</p>
  );

  return (
    <div className="container">
      <h2 className="h4">Wishlist</h2>
      {wishlist.length === 0 ? (
        emptyMessage
      ) : (
        <div className="row">
          <div className="col-sm-12">
            <div className="card table-responsive">
              <table className="table table-hover shopping-cart-wrap">
                <thead className="text-muted">
                  <tr>
                    <th scope="col">Trip</th>
                    <th scope="col" width="120">
                      Price
                    </th>
                    <th scope="col" width="200" className="text-right">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>{itemsMapped}</tbody>
                <tfoot>
                  <tr>
                    <th scope="col" colSpan="2">
                      <dl className="dlist-align">
                        <dt>Total</dt>
                        <dd>{wishlist.length}</dd>
                      </dl>
                    </th>
                    <th scope="col">
                      <button
                        className="btn btn-outline-danger"
                        onClick={clearWishlist}
                        disabled={itemsMapped.length === 0}
                      >
                        Empty Wishlist
                      </button>
                    </th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}